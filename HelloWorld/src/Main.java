import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();

        int lastDigit = number % 10;

        System.out.println("Последняя цифра" + lastDigit);
    }
}